@extends('admin.master.master')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">Fornecedores</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Fornecedores</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <div class="card">
            <div class="row m-2">
                <div class="col-md-2">
                    <a href="{{route('fornecedores.criarNovo')}}" class="btn btn-block btn-info btn-sm">Novo</a>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th style="width: 10px">#</th>
                        <th>Nome</th>
                        <th>CNPJ</th>
                        <th>Telefone</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($fornecedores as $fornecedor)
                        <tr>
                            <td>{{$fornecedor->id}}</td>
                            <td>{{$fornecedor->nome}}</td>
                            <td>{{$fornecedor->telefone}}</td>
                            <td>
                                <div class="btn-group">
                                    <span class="pr-1"><a class="btn btn-block bg-gradient-info btn-sm"
                                                          href="{{route('fornecedores.buscarFornecedorPorId',$fornecedor->id)}}">Editar</a></span>
                                    <form action="{{route('fornecedores.deletarFornecedor',$fornecedor->id)}}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <span><button type="submit" class="btn btn-block bg-gradient-danger btn-sm">Deletar</button></span>
                                    </form>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="float-right">
                    {{$fornecedores->links()}}
                </div>

            </div>
        </div>
@endsection
