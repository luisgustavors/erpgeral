@extends('admin.master.master')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Produto</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <section class="content">
            <div class="container-fluid">
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">Criar Produto</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->

                    <form class="form-horizontal" action="{{route('produtos.store')}}" method="post">
                        @csrf
                        <div class="card-body">
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-2 col-form-label">Nome</label>
                                <div class="col-sm-10">
                                    <input type="text" name="nome" class="form-control" id="nome" placeholder="Nome" value="{{old('nome')}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputPassword3" class="col-sm-2 col-form-label">Valor Custo</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="preco_custo" id="inputpreco_custo"
                                           placeholder="Preço Custo" value="{{old('preco_custo')}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputPassword3" class="col-sm-2 col-form-label">Valor Venda</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="preco_venda" id="inputpreco_venda"
                                           placeholder="Preço Venda" value="{{old('preco_venda')}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputPassword3" class="col-sm-2 col-form-label">Codigo Barras</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="codigo_barra" id="codigo_barra"
                                           placeholder="Barras" value="{{old('codigo_barra')}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputPassword3" class="col-sm-2 col-form-label">Estoque</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="quantidade" id="quantidade"
                                           placeholder="Estoque" value="{{old('quantidade')}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputPassword3" class="col-sm-2 col-form-label">Fornecedor</label>
                                <div class="col-sm-10">
                                    <select name="fornecedor_id" class="form-control" id="fornecedor_id">
                                        <option value="" selected>Selecione um fornecedor</option>
                                        @foreach($fornecedores as $fornecedor)
                                            <option
                                                value="{{$fornecedor->id}}">{{$fornecedor->nome}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputPassword3" class="col-sm-2 col-form-label">Categorias</label>
                                <div class="col-sm-10">
                                    <select name="categoria_id" class="form-control" id="categoria_id">
                                        <option value="" selected>Selecione uma categoria</option>
                                        @foreach($categorias as $categoria)
                                            <option
                                                value="{{$categoria->id}}">{{$categoria->nome}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <button type="submit" class="btn btn-info">Salvar</button>
                        </div>
                        <!-- /.card-footer -->
                    </form>
                </div>
            </div>
        </section>

@endsection
