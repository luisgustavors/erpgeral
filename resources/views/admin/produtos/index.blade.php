@extends('admin.master.master')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">Produtos</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Produtos</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <div class="card">
            <div class="row m-2">
                <div class="col-md-2">
                    <a href="{{route('produtos.criarNovo')}}" class="btn btn-block btn-info btn-sm">Novo</a>
                </div>

                <div class="col-md-6">
                    <form action="{{route('produtos.index')}}" class="d-flex flex-row">
                        <input class="form-control" name="search" type="text" id="customSearch"}}>
                        <button class="btn btn-warning btn-sm">Pesquisar</button>
                    </form>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th style="width: 10px">#</th>
                        <th>Nome</th>
                        <th>Cób. Barra</th>
                        <th>Valor Venda</th>
                        <th>Valor Custo</th>
                        <th>Estoque</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($produtos as $produto)
                        <tr>
                            <td>{{$produto->id}}</td>
                            <td>{{$produto->nome}}</td>
                            <td>{{$produto->codigo_barra}}</td>
                            <td>R$ {{number_format($produto->preco_venda,2,',','.')}}</td>
                            <td>R$ {{number_format($produto->preco_custo,2,',','.')}}</td>
                            <td>{{$produto->quantidade}}</td>
                            <td>
                                <div class="btn-group">
                                    <span class="pr-1"><a class="btn btn-block bg-gradient-info btn-sm"
                                                          href="{{route('produtos.buscarProdutoPorId',$produto->id)}}">Editar</a></span>
                                    <form action="{{route('produtos.deletarProduto',$produto->id)}}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <span><button type="submit" class="btn btn-block bg-gradient-danger btn-sm">Deletar</button></span>
                                    </form>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="float-right">
                    {{$produtos->links()}}
                </div>

            </div>
        </div>
@endsection
