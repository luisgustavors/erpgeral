@extends('admin.master.master')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Clientes</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->


        <section class="content">
            <div class="container-fluid">
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">Editar Cliente</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form class="form-horizontal" action="{{route('cliente.atualizaCliente',$cliente->id)}}"
                          method="post">
                        @csrf
                        @method('PUT')
                        <div class="card-body">
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-2 col-form-label">Nome</label>
                                <div class="col-sm-10">
                                    <input type="text" name="nome" class="form-control" id="nome" placeholder="Nome"
                                           value="{{$cliente->nome}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputPassword3" class="col-sm-2 col-form-label">Telefone</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="telefone" id="inputTelefone"
                                           placeholder="Telefone" value="{{$cliente->telefone}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputPassword3" class="col-sm-2 col-form-label">CPF/CNPJ</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="cpfcnpj" id="inputcpfcnpj"
                                           placeholder="cpfcnpj" value="{{$cliente->cpfcnpj}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputPassword3" class="col-sm-2 col-form-label">País</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="pais" id="pais" placeholder="País"
                                           value="{{$cliente->pais}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputPassword3" class="col-sm-2 col-form-label">Estado</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="estado" id="estado"
                                           placeholder="Estado" value="{{$cliente->estado}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputPassword3" class="col-sm-2 col-form-label">Cidade</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="cidade" id="cidade"
                                           placeholder="Cidade" value="{{$cliente->cidade}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputPassword3" class="col-sm-2 col-form-label">Cidade</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="cidade" id="cidade"
                                           placeholder="Cidade" value="{{$cliente->cidade}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputPassword3" class="col-sm-2 col-form-label">Perfil</label>
                                <div class="col-sm-10">
                                    <select name="tipopessoa" class="form-control" id="tipopessoa">
                                        <option value="">Selecione um Valor</option>
                                        @foreach($perfils as $perfil)
                                            <option
                                                value="{{$perfil->tipopessoa}}" {{($perfil->id) == $cliente->perfil_id ? 'selected' : ''}}>{{$perfil->nome}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputPassword3" class="col-sm-2 col-form-label">Tipo Pessoa</label>
                                <div class="col-sm-10">
                                    <select name="tipopessoa" class="form-control" id="tipopessoa">
                                        <option value="">Selecione um Valor</option>
                                        <option value="F" {{($cliente->tipopessoa) == 'Fisíca' ? 'selected' : ''}}>
                                            Fisíca
                                        </option>
                                        <option value="J" {{($cliente->tipopessoa) == 'Jurídico' ? 'selected' : ''}}>
                                            Jurídica
                                        </option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <button type="submit" class="btn btn-info">Atualizar</button>
                        </div>
                        <!-- /.card-footer -->
                    </form>
                </div>
            </div>
        </section>

@endsection
