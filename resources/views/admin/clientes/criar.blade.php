@extends('admin.master.master')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Clientes</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <section class="content">
            <div class="container-fluid">
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">Novo Cliente</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->

                    <form class="form-horizontal" action="{{route('cliente.store')}}" method="post">
                        @csrf
                        <div class="card-body">
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-2 col-form-label">Nome</label>
                                <div class="col-sm-10">
                                    <input type="text" name="nome" class="form-control" id="nome" placeholder="Nome" value="{{old('nome')}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputPassword3" class="col-sm-2 col-form-label">Telefone</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="telefone" id="inputTelefone"
                                           placeholder="Telefone" value="{{old('telefone')}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputPassword3" class="col-sm-2 col-form-label">CPF/CNPJ</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="cpfcnpj" id="inputcpfcnpj"
                                           placeholder="cpfcnpj" value="{{old('cpfcnpj')}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputPassword3" class="col-sm-2 col-form-label">País</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="pais" id="pais" placeholder="País" value="{{old('pais')}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputPassword3" class="col-sm-2 col-form-label">Estado</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="estado" id="estado"
                                           placeholder="Estado" value="{{old('estado')}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputPassword3" class="col-sm-2 col-form-label">Cidade</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="cidade" id="cidade"
                                           placeholder="Cidade" value="{{old('cidade')}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputPassword3" class="col-sm-2 col-form-label">Logradouro</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="logradouro" id="logradouro"
                                           placeholder="logradouro" value="{{old('logradouro')}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputPassword3" class="col-sm-2 col-form-label">Perfil</label>
                                <div class="col-sm-10">
                                    <select name="tipopessoa" class="form-control" id="tipopessoa">
                                        <option value="" selected>Selecione um Valor</option>
                                        @foreach($perfils as $perfil)
                                            <option
                                                value="{{$perfil->tipopessoa}}">{{$perfil->nome}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputPassword3" class="col-sm-2 col-form-label">Tipo Pessoa</label>
                                <div class="col-sm-10">
                                    <select name="tipopessoa" class="form-control" id="tipopessoa">
                                        <option value="" selected>Selecione um Valor</option>
                                        <option value="F">Fisíca</option>
                                        <option value="J">Jurídica</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <button type="submit" class="btn btn-info">Salvar</button>
                        </div>
                        <!-- /.card-footer -->
                    </form>
                </div>
            </div>
        </section>

@endsection
