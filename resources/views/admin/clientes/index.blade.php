@extends('admin.master.master')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">Clientes</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Clientes</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <div class="card">
            <div class="row m-2">
                <div class="col-md-2">
                    <a href="{{route('cliente.criarNovo')}}" class="btn btn-block btn-info btn-sm">Novo</a>
                </div>

                <div class="col-md-6">
                    <form action="{{route('cliente.index')}}" class="d-flex flex-row">
                        <input class="form-control" name="search" type="text" id="customSearch"}}>
                        <button class="btn btn-warning btn-sm">Pesquisar</button>
                    </form>
                </div>
            </div>

            <div class="row m-2">

            </div>
            <div class="card-body">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th style="width: 10px">#</th>
                        <th>Nome</th>
                        <th>Tipo</th>
                        <th>Telefone</th>
                        <th>Ações</th>
                        <!-- <th></th> -->
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($clientes as $cliente)
                        <tr>
                            <td>{{$cliente->id}}</td>
                            <td>{{$cliente->nome}}</td>
                            <td>{{$cliente->tipopessoa}}</td>
                            <td>{{$cliente->telefone}}</td>
                            <td>
                                <div class="btn-group">
                                    <span class="pr-1"><a class="btn btn-block bg-gradient-info btn-sm"
                                                          href="{{route('cliente.editarCliente',$cliente->id)}}">Editar</a></span>
                                    <form action="{{route('cliente.deletarCliente',$cliente->id)}}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <span><button type="submit" class="btn btn-block bg-gradient-danger btn-sm">Deletar</button></span>
                                    </form>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="float-right">
                    {{$clientes->links()}}
                </div>

            </div>
        </div>
@endsection
