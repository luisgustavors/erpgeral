@extends('admin.master.master')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Categoria</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->


        <section class="content">
            <div class="container-fluid">
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">Editar Categoria</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form class="form-horizontal" action="{{route('categoria.atualizarCategoria',$categoria->id)}}" method="post">
                        @csrf
                        @method('PUT')
                        <div class="card-body">
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-2 col-form-label">Nome</label>
                                <div class="col-sm-10">
                                    <input type="text" name="nome" class="form-control" id="nome" placeholder="Nome" value="{{$categoria->nome}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputPassword3" class="col-sm-2 col-form-label">Desconto</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="desconto" id="inputDesconto"
                                           placeholder="Desconto" value="{{$categoria->desconto}}">
                                </div>
                            </div>

                            <div class="custom-control custom-checkbox">
                                <input class="custom-control-input" name="ativo" type="checkbox" id="customCheckbox2" {{($categoria->ativo) == 1 || $categoria->ativo === true ? 'checked' : ''}}>
                                <label for="customCheckbox2" class="custom-control-label">Ativo</label>
                            </div>

                            <div class="form-group row">
                                <label for="inputPassword3" class="col-sm-2 col-form-label">Perfil</label>
                                <div class="col-sm-10">
                                    <select name="perfil_id" class="form-control" id="perfil_id">
                                        <option value="">Selecione um Valor</option>
                                        @foreach($perfils as $perfil)
                                            <option
                                                value="{{$perfil->id}}" {{($perfil->id) == $categoria->perfil_id ? 'selected' : ''}}>{{$perfil->nome}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <button type="submit" class="btn btn-info">Atualizar</button>
                        </div>
                        <!-- /.card-footer -->
                    </form>
                </div>
            </div>
        </section>

@endsection
