@extends('admin.master.master')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Venda</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <section class="content">
            <div class="container-fluid">
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">Criar Venda</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->

                    <form class="form-horizontal" action="{{route('pedidovenda.store')}}" method="post">
                        @csrf
                        <div class="card-body">

                            <div class="form-group row">
                                <label for="inputPassword3" class="col-sm-2 col-form-label">Clientes</label>
                                <div class="col-sm-10">
                                    <select name="cliente_id" class="form-control" id="cliente_id" required>
                                        <option value="" selected>Selecione um cliente</option>
                                        @foreach($clientes as $cliente)
                                            <option
                                                value="{{$cliente->id}}">{{$cliente->nome}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <table class="table table-bordered" id="dynamic_field">
                            <thead>
                            <th>Produto</th>
                            <th>Valor</th>
                            <th>Estoque</th>
                            <th><input type="button" name="submit" id="submit" class="btn btn-info addRow" value="+"/></th>
                            </thead>
                            <tr>
                                <td>
                                    {{--                                                        <label>Produto</label>--}}
                                    <select name="produto_id[]" onchange="retornaDados()" required class="form-control select2bs4 productTable" style="width: 100%;">
                                        <option value="" selected>Selecione um Produto</option>
                                        @foreach($produtos as $produto)
                                            <option value="{{$produto->id}}" data-produto="{{$produto->id}}">{{$produto->nome}}</option>
                                        @endforeach
                                    </select>
                                </td>

                                <td>
                                    {{--                                                        <label for="safv_valorunidade">Valor unitário</label>--}}
                                    <input type="text" class="form-control moeda valorunidade" name="valor[]" onkeyup="valorTotal()" required>
                                </td>

                                <td>
                                    {{--                                                        <label for="safv_volumetotal">Volume</label>--}}
                                    <input type="number" class="form-control volume estoque" name="qtd[]" onkeyup="valorTotal()" required>
                                    <div class="alerta" style="width: 100%;">
                                    </div>
                                </td>

                                <td>
                                    <input type="button" name="submit" id="submit" class="btn btn-danger remove" value="-"/>
                                </td>
                            </tr>
                        </table>

                        <div class="col-md-4 col-sm-4">
                            <div class="form-group">
                                <label for="total">Total venda R$</label>
                                <input type="text" class="form-control moeda" id="valortotal" name="total" readonly>
                            </div>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <button type="submit" class="btn btn-info">Salvar</button>
                        </div>
                        <!-- /.card-footer -->
                    </form>
                </div>
            </div>
        </section>

        @endsection

        @section('js')
            <script>
                $(function () {
                    $('.addRow').click(function () {
                        addRow();
                    });

                    $('tbody').on('click', '.remove', function () {
                        $(this).parent().parent().remove()
                        valorTotal()
                    })

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                });

                function addRow() {
                    var tr = '<tr>' +
                        '<td>' +
                        '<select name="produto_id[]" onchange="retornaDados()" required class="form-control select2bs4 productTable" style="width: 100%;">' +
                        '<option value="" selected>Selecione um Produto</option>'+
                        @foreach($produtos as $produto)
                            '<option value="{{$produto->id}}" data-produto="{{$produto->id}}">{{$produto->nome}}</option>' +
                        @endforeach
                            '</select>' +
                        '</td>' +
                        '<td>' +
                        '<input type="text" class="form-control moeda valorunidade"  name="valor[]" onkeyup="valorTotal()" required>' +
                        '</td>' +
                        '<td>' +
                        '<input type="number" class="form-control volume estoque" name="qtd[]" onkeyup="valorTotal()" required>' +
                        '</td>' +
                        '<td>' +
                        '<input type="button" name="submit" id="submit" class="btn btn-danger remove" value="-"/>' +
                        '</td>' +
                        '</tr>';

                    $('tbody').append(tr)
                }

                function valorTotal() {
                    debugger
                    console.log($('.valorunidade').val())
                    var totalGeral = 0;
                    var table = $('#dynamic_field');
                    table.find('tbody tr').each(function (index, value) {
                        var valUnit = $(this).find('.valorunidade').val().replace('R$', '').replace('.', '').replace(',', '.');
                        var valUnidade = $(this).find('.estoque').val()
                        totalGeral += parseFloat(valUnit) * parseFloat(valUnidade);
                    })
                    if (!isNaN(totalGeral)) {
                        $('#valortotal').val(formatarMoeda(totalGeral))
                    }
                }

                function retornaDados() {
                    $('#dynamic_field').find('tbody tr').each(function (i, v) {
                            var produto = $(this).find('.productTable option:selected').attr('data-produto')
                            var volume = $(this).find('.safv_volumetotal').val()
                            $.ajax({
                                url: '{{route('pedidovenda.buscaprod')}}',
                                method: 'get',
                                data: {id: produto, qtd: volume},
                                success: function (response) {
                                    var itens = response.data;
                                    console.log(response)
                                    if (itens != false) {
                                        if (produto == itens.id) {
                                            $('.valorunidade').eq(i).val(formatarMoeda(itens.preco_venda))
                                        }
                                    }

                                },
                                error: function (error) {
                                }
                            });
                        }
                    )
                }

                function formatarMoeda(valor) {
                    var valorFormatado = new Intl.NumberFormat('pt-BR', {
                        style: 'currency',
                        currency: 'BRL',
                    }).format(valor);

                    return valorFormatado;
                }
            </script>
@endsection
