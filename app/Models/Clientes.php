<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Casts\Attribute;


class Clientes extends Model
{
    use HasFactory, UuidTrait;

    public $incrementing = false;
    protected $keyType = 'cod';


    protected $fillable = [
        'tipopessoa',
        'nome',
        'telefone',
        'cpfcnpj',
        'pais',
        'estado',
        'cidade',
        'bairro',
        'logradouro',
        'endereco',
        'numero',
        'cep',
        'email',
    ];

    public function tipopessoa(): Attribute
    {
        return Attribute::make(
            get: fn($value) => ($value) == 'F' ? 'Fisíca' : 'Jurídico'
        );
    }

    public function vendas()
    {
        return $this->hasOne(PedidoVenda::class);
    }
}
