<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\UuidTrait;

class Cargos extends Model
{
    use HasFactory;

    protected $fillable = [
        'nome',
        'perfil_id',
    ];

    public function perfil()
    {
        return $this->belongsTo(Perfils::class);
    }
}
