<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\UuidTrait;

class Categorias extends Model
{
    use HasFactory;

    protected $fillable = [
        'nome',
        'desconto',
        'ativo',
        'perfil_id'
    ];
}
