<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PedidoVenda extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'cliente_id',
        'user_id',
        'total'
    ];

    public function cliente()
    {
        return $this->belongsTo(Clientes::class);
    }
}
