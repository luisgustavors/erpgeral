<?php

namespace App\Repositories;

use App\Models\User;

class UsuarioRepository
{
    protected $model;

    public function __construct(User $model)
    {
        $this->model = $model;
    }

    public function criarUsuario(array $data)
    {
        $data['password'] = bcrypt($data['password']);
        return $this
            ->model
            ->create($data);
    }

    public function buscarUsuarioFiltro(array $filtros)
    {
        return $this
            ->model
            ->when($filtros == 'nome', function ($query, $filtros) {
                $query->where('nome', $filtros['nome']);
            })
            ->when($filtros == 'telefone', function ($query, $filtros) {
                $query->where('telefone', $filtros['telefone']);
            })
            ->when($filtros == 'cpfcnpj', function ($query, $filtros) {
                $query->where('cpfcnpj', $filtros['cpfcnpj']);
            })->get();
    }

    public function buscarUsuarioPorId($id)
    {
        return $this
            ->model
            ->where('id', $id)->first();
    }

    public function atualizarUsuario($id,$data)
    {
        $data['password'] = bcrypt($data['password']);
        return $this
            ->model
            ->where('id',$id)
            ->update($data);
    }

    public function buscarTodosUsuario()
    {
        return $this
            ->model
            ->with(['perfil'])
            ->paginate();
    }

    public function deletarUsuario($id)
    {
        return $this->model->where('id', $id)->delete();
    }
}
