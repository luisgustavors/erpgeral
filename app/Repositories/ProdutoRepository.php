<?php

namespace App\Repositories;

use App\Models\Produtos;

class ProdutoRepository
{
    protected $model;

    public function __construct(Produtos $model)
    {
        $this->model = $model;
    }

    public function criarProduto(array $data)
    {
        if (!empty($data['preco_custo'])) {
            $data['preco_custo'] = str_replace(',', '.', str_replace('.', '', $data['preco_custo']));
            $data['user_id'] = auth()->user()->id;
        }
        if (!empty($data['preco_venda'])) {
            $data['preco_venda'] = str_replace(',', '.', str_replace('.', '', $data['preco_venda']));
            $data['user_id'] = auth()->user()->id;
        }

        return $this
            ->model
            ->create($data);
    }

    public function buscarProdutoFiltro($filtros)
    {
        return $this
            ->model
            ->where('nome', 'like', '%' . $filtros . '%')
            ->paginate();
    }

    public function buscarProdutoPorId($id)
    {
        return $this
            ->model
            ->where('id', $id)->first();
    }

    public function atualizarProduto($id, $data)
    {
        return $this
            ->model
            ->where('id', $id)
            ->update($data);
    }

    public function buscarTodosProduto()
    {
        return $this
            ->model
            ->paginate();
    }

    public function deletarProduto($cod)
    {
        $this->model->where('id', $cod)->delete();

        return response()->json(['mensagem' => 'Produto deletado com sucesso!!!']);
    }
}
