<?php

namespace App\Repositories;

use App\Models\Clientes;

class ClienteRepository
{
    protected $model;

    public function __construct(Clientes $model)
    {
        $this->model = $model;
    }

    public function criarCliente(array $data)
    {
        $data['user_id'] = auth()->user()->id;
        return $this
            ->model
            ->create($data);
    }

    public function buscarClienteFiltro($filtros)
    {
        return $this
            ->model
            ->where('nome', 'like', '%'.$filtros.'%')
            ->paginate();
    }

    public function buscarClientePorId($id)
    {
        return $this
            ->model
            ->where('id', $id)->first();
    }

    public function atualizarCliente($id, $data)
    {
        return $this
            ->model
            ->where('id', $id)
            ->update($data);
    }

    public function buscarTodosCliente()
    {
        return $this
            ->model
            ->paginate();
    }

    public function deletarCliente($id)
    {
        return $this->model->where('id', $id)->delete();
    }
}
