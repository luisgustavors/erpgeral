<?php

namespace App\Repositories;

use App\Models\Fornecedores;

class FornecedorRepository
{
    protected $model;

    public function __construct(Fornecedores $model)
    {
        $this->model = $model;
    }

    public function criarFornecedor(array $data)
    {
        return $this
            ->model
            ->create($data);
    }

    public function buscarFornecedorFiltro(array $filtros)
    {
        return $this
            ->model
            ->when($filtros == 'nome', function ($query, $filtros) {
                $query->where('nome', $filtros['nome']);
            });
    }

    public function atualizarFornecedor($id,$data)
    {
        return $this
            ->model
            ->where('id',$id)
            ->update($data);
    }

    public function buscarFornecedorPorId($id)
    {
        return $this
            ->model
            ->where('id', $id)->first();
    }

    public function buscarTodosFornecedor()
    {
        return $this
            ->model
            ->paginate();
    }

    public function deletarFornecedor($id)
    {
        $this->model->where('id', $id)->delete();

        return response()->json(['mensagem' => 'Fornecedor deletado com sucesso!!!']);
    }
}
