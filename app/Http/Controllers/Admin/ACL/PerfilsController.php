<?php

namespace App\Http\Controllers\Admin\ACL;

use App\Http\Controllers\Controller;
use App\Http\Requests\PerfilRequest;
use App\Http\Resources\PerfilsResource;
use App\Repositories\PerfilRepository;
use Illuminate\Http\Request;

class PerfilsController extends Controller
{
    private $perfil;

    public function __construct(PerfilRepository $model)
    {
        $this->perfil = $model;
    }

    public function index()
    {
        $perfils = $this->perfil->buscarTodosPerfil();
        return view('admin.perfils.index', compact('perfils'));
    }

    public function criarNovo()
    {
        return view('admin.perfils.criar');
    }

    public function atualizarPerfils(Request $request, $id)
    {
        $this->perfil->atualizarPerfil($id, $request->except('_token', '_method'));
        return redirect()->route('perfil.index');
    }

    public function buscarPerfilPorId($id)
    {
        $perfil = $this->perfil->buscarPerfilPorId($id);
        return view('admin.perfils.editar',compact('perfil'));
    }

    public function store(PerfilRequest $request)
    {
        $this->perfil->criarPerfil($request->validated());
        return redirect()->route('perfil.index');
    }

    public function deletarPerfil($id)
    {
        $this->perfil->deletarPerfil($id);
        return redirect()->route('perfil.index');
    }
}
