<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Perfils;
use App\Repositories\UsuarioRepository;
use Illuminate\Http\Request;

class UsuarioController extends Controller
{
    private $usuario, $perfils;

    public function __construct(UsuarioRepository $model, Perfils $perfils)
    {
        $this->usuario = $model;
        $this->perfils = $perfils;
    }

    public function index()
    {
        $usuarios = $this->usuario->buscarTodosUsuario();
        //dd($usuarios);
        return view('admin.usuarios.index', compact('usuarios'));
    }

    public function criarNovo()
    {
        $usuario = $this->usuario->buscarTodosUsuario();
        $perfils = $this->perfils->all();
        return view('admin.usuarios.criar', compact('usuario','perfils'));
    }

    public function buscarUsuarioPorId($id)
    {
        $usuario = $this->usuario->buscarUsuarioPorId($id);
        $perfils = $this->perfils->all();
        return view('admin.usuarios.editar', compact('usuario','perfils'));
    }

    public function atualizarUsuario(Request $request, $id)
    {
        $this->usuario->atualizarUsuario($id, $request->except('_token', '_method'));
        return redirect()->route('usuario.index');
    }

    public function store(Request $request)
    {
        $this->usuario->criarUsuario($request->all());
        return redirect()->route('usuario.index');
    }

    public function deletarUsuario($id)
    {
        $this->usuario->deletarUsuario($id);
        return redirect()->route('usuario.index');
    }
}
