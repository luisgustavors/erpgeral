<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CategoriaRequest;
use App\Http\Resources\CategoriasResource;
use App\Models\Perfils;
use App\Repositories\CategoriaRepository;
use Illuminate\Http\Request;

class CategoriasController extends Controller
{
    private $categoria, $perfil;

    public function __construct(CategoriaRepository $model, Perfils $perfil)
    {
        $this->categoria = $model;
        $this->perfil = $perfil;
    }

    public function index()
    {
        $categorias = $this->categoria->buscarTodosCategoria();
        return view('admin.categorias.index', compact('categorias'));
    }

    public function buscarCategoriaPorId($id)
    {
        $categoria = $this->categoria->buscarCategoriaPorId($id);
        $perfils = $this->perfil->all();
        return view('admin.categorias.editar', compact('categoria','perfils'));
    }

    public function criarNovo()
    {
        $perfils = $this->perfil->all();
        return view('admin.categorias.criar', compact('perfils'));
    }

    public function atualizarCategoria(Request $request, $id)
    {
        //dd($request->all());
        $this->categoria->atualizarCategoria($id, $request->except('_token', '_method'));
        return redirect()->route('categoria.index');
    }

    public function store(CategoriaRequest $request)
    {
        $categoria = $this->categoria->criarCategoria($request->validated());
        return redirect()->route('categoria.index');
    }

    public function deletarCategoria($id)
    {
        $this->categoria->deletarCategoria($id);
        return redirect()->route('categoria.index');
    }
}
