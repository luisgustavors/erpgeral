<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ClienteRequest;
use App\Http\Resources\ClientesResource;
use App\Models\Perfils;
use App\Repositories\ClienteRepository;
use Exception;
use Illuminate\Http\Request;

class ClientesController extends Controller
{
    private $cliente, $perfil;

    public function __construct(ClienteRepository $model, Perfils $perfil)
    {
        $this->cliente = $model;
        $this->perfil = $perfil;
    }

    public function index(Request $request)
    {
        if ($request->get('search')) {
            $clientes = $this->cliente->buscarClienteFiltro($request->get('search'));
        } else {
            $clientes = $this->cliente->buscarTodosCliente();
        }

        return view('admin.clientes.index', compact('clientes'));
    }

    public function buscarClientePorId($id)
    {
        $cliente = $this->cliente->buscarClientePorId($id);
        $perfils = $this->perfil->all();
        return view('admin.clientes.editar', compact('cliente', 'perfils'));
    }

    public function criarNovo()
    {
        $perfils = $this->perfil->all();
        return view('admin.clientes.criar', compact('perfils'));
    }

    public function store(ClienteRequest $request)
    {
        $cliente = $this->cliente->criarCliente($request->validated());
        return redirect()->route('cliente.index');
    }

    public function atualizarCliente(Request $request, $id)
    {
        $this->cliente->atualizarCliente($id, $request->except('_token', '_method'));
        return redirect()->route('cliente.index');
    }

    public function deletarCliente($id)
    {
        try {
            $this->cliente->deletarCliente($id);
            return redirect()->route('cliente.index');
        } catch (Exception $e) {
            return 'Erro ao deletar ' . $e;
        }
    }
}
