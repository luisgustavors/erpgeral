<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Clientes;
use App\Models\ItensVenda;
use App\Models\PedidoVenda;
use App\Models\Produtos;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PedidoVendaController extends Controller
{
    private $pedidoVenda, $itensVenda, $clientes, $produtos;

    public function __construct(PedidoVenda $pedidoVenda, ItensVenda $itensVenda, Clientes $clientes, Produtos $produtos)
    {
        $this->itensVenda = $itensVenda;
        $this->pedidoVenda = $pedidoVenda;
        $this->clientes = $clientes;
        $this->produtos = $produtos;
    }

    public function index()
    {
        $vendas = $this->pedidoVenda->with('cliente')->paginate();
        // dd($vendas);
        return view('admin.venda.index', compact('vendas'));
    }

    public function criarNovo()
    {
        $clientes = $this->clientes->all();
        $produtos = $this->produtos->all();
        return view('admin.venda.criar', compact('clientes', 'produtos'));
    }

    public function store(Request $request)
    {

        $pedidoVenda = $this->pedidoVenda->create([
            'cliente_id' => $request->cliente_id,
            'user_id' => auth()->user()->id,
            'total' => $this->convertStringToDouble($request->total)
        ]);

        $produto = $request->produto_id;
        $valor = $request->valor;
        $qtd = $request->qtd;
        for ($i = 0; $i < count($produto); $i++) {
            $produtoEst = $this->produtos->find($produto[$i]);
            $produtoEst->decrement('quantidade', $qtd[$i]);

            ItensVenda::create(array(
                'produto_id' => $produto[$i],
                'valor' => $this->convertStringToDouble($valor[$i]),
                'qtd' => $qtd[$i],
                'pedido_id' => $pedidoVenda->id,
            ));
        }

        return redirect()->route('pedidovenda.index');
    }

    public function buscaprod(Request $request)
    {
        $produto = $this->produtos->find($request->id);
        return response()->json(['data' => $produto]);
    }

    public function verificaEstoque(Request $request)
    {
        $produto = $this->produtos->where('id', $request->id)
            ->select('id','nome','quantidade')
            ->first();

        if (doubleval($produto->quantidade) >= $request->qtd) {
            return response()->json(['data' => false]);
        }else{
            return response()->json([
                'data' => $produto,
            ]);
        }
    }

    private function convertStringToDouble($param)
    {
        if (empty($param)) {
            return null;
        }
        $novoValor = str_replace('R$', '', $param);

        return str_replace(',', '.', str_replace('.', '', $novoValor));
    }
}
