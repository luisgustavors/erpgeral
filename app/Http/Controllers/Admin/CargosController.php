<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CargoRequest;
use App\Http\Resources\CargosResource;
use App\Models\Perfils;
use App\Repositories\CargoRepository;
use Illuminate\Http\Request;


class CargosController extends Controller
{
    private $cargo, $perfil;

    public function __construct(CargoRepository $model, Perfils $perfil)
    {
        $this->cargo = $model;
        $this->perfil = $perfil;
    }

    public function index()
    {
        $cargos = $this->cargo->buscarTodosCargo();
        return view('admin.cargos.index', compact('cargos'));
    }

    public function criarNovo()
    {
        $perfils = $this->perfil->all();
        return view('admin.cargos.criar', compact('perfils'));
    }

    public function buscarCargoPorId($id)
    {
        $cargo = $this->cargo->buscarCargoPorId($id);
        $perfils = $this->perfil->all();
        return view('admin.cargos.editar', compact('cargo','perfils'));
    }

    public function atualizarCargo(Request $request, $id)
    {
        $this->cargo->atualizarCargo($id, $request->except('_token', '_method'));
        return redirect()->route('cargos.index');
    }

    public function store(CargoRequest $request)
    {
        $cargo = $this->cargo->criarCargo($request->validated());
        return redirect()->route('cargos.index');
    }

    public function deletarCargo($id)
    {
        $cargo = $this->cargo->deletarCargo($id);
        return redirect()->route('cargos.index');
    }
}
