<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\FornecedorRequest;
use App\Http\Resources\FornecedoresResource;
use App\Repositories\FornecedorRepository;
use Illuminate\Http\Request;

class FornecedoresController extends Controller
{
    private $fornecedor;

    public function __construct(FornecedorRepository $model)
    {
        $this->fornecedor = $model;
    }

    public function index()
    {
        $fornecedores = $this->fornecedor->buscarTodosFornecedor();
        return view('admin.fornecedores.index',compact('fornecedores'));
    }

    public function buscarFornecedorPorId($id)
    {
        $fornecedor = $this->fornecedor->buscarFornecedorPorId($id);
        return view('admin.fornecedores.editar',compact('fornecedor'));
    }

    public function atualizarFornecedor(Request $request, $id)
    {
        $this->fornecedor->atualizarFornecedor($id, $request->except('_token','_method'));
        return redirect()->route('fornecedores.index');
    }

    public function criarNovo()
    {
        return view('admin.fornecedores.criar');
    }

    public function store(Request $request)
    {
        $fornecedor = $this->fornecedor->criarFornecedor($request->all());
        return redirect()->route('fornecedores.index');
    }

    public function deletarFornecedor($id)
    {
        $fornecedor = $this->fornecedor->deletarFornecedor($id);
        return redirect()->route('fornecedores.index');
    }
}
