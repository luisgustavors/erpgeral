<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProdutoRequest;
use App\Http\Resources\ProdutosResource;
use App\Models\Categorias;
use App\Models\Fornecedores;
use App\Repositories\ProdutoRepository;
use Illuminate\Http\Request;

class ProdutosController extends Controller
{
    private $produto, $categoria, $fornecedor;

    public function __construct(ProdutoRepository $model, Fornecedores $fornecedor, Categorias $categoria)
    {
        $this->produto = $model;
        $this->fornecedor = $fornecedor;
        $this->categoria = $categoria;
    }

    public function index(Request $request)
    {
        if ($request->get('search')) {
            $produtos = $this->produto->buscarProdutoFiltro($request->get('search'));
        } else {
            $produtos = $this->produto->buscarTodosProduto();
        }

        return view('admin.produtos.index', compact('produtos'));
        // return view('login', compact('produtos'));
    }

    public function buscarProdutoPorId($id)
    {

        $produto = $this->produto->buscarProdutoPorId($id);
        $categorias = $this->categoria->all();
        $fornecedores = $this->fornecedor->all();
        return view('admin.produtos.editar', compact('produto','categorias','fornecedores'));
    }

    public function criarNovo()
    {
        $categorias = $this->categoria->all();
        $fornecedores = $this->fornecedor->all();
        return view('admin.produtos.criar', compact('categorias', 'fornecedores'));
    }

    public function atualizarProduto(Request $request, $id)
    {
        $this->produto->atualizarProduto($id, $request->except('_token', '_method'));
        return redirect()->route('produtos.index');
    }

    public function store(Request $request)
    {
        $this->produto->criarProduto($request->all());
        return redirect()->route('produtos.index');
    }

    public function deletarProduto($cod)
    {
        $produto = $this->produto->deletarProduto($cod);
        return redirect()->route('produtos.index');
    }
}
