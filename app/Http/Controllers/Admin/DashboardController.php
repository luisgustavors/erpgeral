<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\PedidoVenda;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{

    public function index()
    {
//        $produtos = DB::table('itens_vendas')
//            ->join('produtos', 'itens_vendas.produto_id', '=', 'produtos.id')
//            ->join('categorias', 'categorias.id', '=', 'produtos.categoria_id')
//            ->groupBy('categorias.nome')
//            ->select('categorias.nome',DB::raw('SUM(itens_vendas.qtd) as qtd'))
//            ->get();

        $produtos = DB::select(DB::raw('select categorias.nome, SUM(itens_vendas.qtd) as qtd from itens_vendas inner join
produtos on itens_vendas.produto_id = produtos.id inner join categorias on categorias.id = produtos.categoria_id group by categorias.nome'));

        $chartData = "";
        foreach ($produtos as $produto)
        {
            $chartData.="['".$produto->nome."',".$produto->qtd."],";
        }
        $chartData = rtrim($chartData,",");
        //dd($chartData);

        return view('admin.dashboard.dashboard', compact('chartData'));
    }
}
