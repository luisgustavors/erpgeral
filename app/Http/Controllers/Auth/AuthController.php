<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Resources\UsuarioResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function auth(Request $request)
    {
        // PARA CRIPTOGRAFAR A SENHA DO USER NO BD
        // $user = User::first();
        // $user->password=bcrypt('login@upf.br');
        // $user->save();

        // fim


        $request->validate([
            'email' => 'required|email',
            'password' => 'required',
            'device_name' => 'required',
        ]);

        $client = User::where('email', $request->email)->first();

        if (!$client || !Hash::check($request->password, $client->password)) {
            return response()->json(['message' => 'Credenciais inválidas'], 404);
        }

        $token = $client->createToken($request->device_name)->plainTextToken;

        return response()->json(['token' => $token]);
    }

    public function me(Request $request)
    {
        $usuario = $request->user();

        return new UsuarioResource($usuario);
    }

    // public function logout()
    // {
    //     Auth::logout();
    //     return redirect()->route('login');
    // }
    //   public function logout()
    // {
    //     Auth::logout();
    //     return redirect('/login');
    // }
    public function logout(){
        Auth::logout();
        return redirect()->route('login');
    }
}
