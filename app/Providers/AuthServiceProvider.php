<?php

namespace App\Providers;

use Illuminate\Auth\Access\Response;
use App\Models\User;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The model to policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();


        Gate::define('gerente', function (User $user) {

            if ($user->perfil->nome === 'Gerente da empresa' || $user->perfil->nome === 'Administrador') {
                return Response::allow();
            } else {
                return Response::deny('Você deve ser o Gerente da Empresa');
            }
        });

        Gate::define('vendedor', function (User $user) {

            if ($user->perfil->nome === 'Vendedor' || $user->perfil->nome === 'Gerente da empresa' || $user->perfil->nome === 'Administrador') {
                return Response::allow();
            } else {
                return Response::deny('Você não está autorizado');
            }
        });

        Gate::define('adm', function (User $user) {

            if ($user->perfil->nome === 'Administrador') {
                return Response::allow();
            }
        });
    }
}
