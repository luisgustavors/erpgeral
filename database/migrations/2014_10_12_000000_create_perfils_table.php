<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('perfils', function (Blueprint $table) {
            $table->id();
            $table->string('nome', 45);
            $table->string('descricao', 130);
            $table->timestamps();
        });

       DB::table('perfils')->insert(
            ['nome' => 'Administrador', 'descricao' => 'Administrador do sistema']
        );
        DB::table('perfils')->insert(
            ['nome' => 'Gerente', 'descricao' => 'Gerente da empresa']
        );
        DB::table('perfils')->insert(
            ['nome' => 'Vendedor', 'descricao' => 'Vendedor da loja']
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('perfils');
    }
};
