<?php

use App\Http\Controllers\Admin\ACL\PerfilsController;
use App\Http\Controllers\Admin\CargosController;
use App\Http\Controllers\Admin\CategoriasController;
use App\Http\Controllers\Admin\ClientesController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\FornecedoresController;
use App\Http\Controllers\Admin\PlanosController;
use App\Http\Controllers\Admin\ProdutosController;
use App\Http\Controllers\Admin\UsuarioController;
use App\Http\Controllers\Admin\PedidoVendaController;
use App\Http\Controllers\Auth\AuthController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
});

Route::middleware(['auth', 'verified'])->group(function () {
    Route::controller(ClientesController::class)->group(function () {
        Route::any('/clientes', 'index')->name('cliente.index');
        Route::get('clientes/{id}', 'buscarClientePorId')->name('cliente.editarCliente');
        Route::put('/clientes/{id}', 'atualizarCliente')->name('cliente.atualizaCliente');
        Route::post('/clientes', 'store')->name('cliente.store');
        Route::get('/novocliente', 'criarNovo')->name('cliente.criarNovo');
        Route::delete('/clientes/{cod}', 'deletarCliente')->name('cliente.deletarCliente');
    });

    Route::middleware('can:gerente,adm')->group(function () {
        Route::controller(FornecedoresController::class)->group(function () {
            Route::get('/fornecedores', 'index')->name('fornecedores.index');
            Route::get('/fornecedores/{id}', 'buscarFornecedorPorId')->name('fornecedores.buscarFornecedorPorId');
            Route::put('/fornecedores/{id}', 'atualizarFornecedor')->name('fornecedores.atualizarFornecedor');
            Route::post('/fornecedores', 'store')->name('fornecedores.store');
            Route::get('/novofornecedor', 'criarNovo')->name('fornecedores.criarNovo');
            Route::delete('/fornecedores/{id}', 'deletarFornecedor')->name('fornecedores.deletarFornecedor');
        });
    });

    Route::middleware('can:vendedor,gerente,adm')->group(function () {
        Route::controller(CategoriasController::class)->group(function () {
            Route::get('/categorias', 'index')->name('categoria.index');
            Route::get('/categorias/{id}', 'buscarCategoriaPorId')->name('categoria.buscarCategoriaPorId');
            Route::put('/categorias/{id}', 'atualizarCategoria')->name('categoria.atualizarCategoria');
            Route::post('/categorias', 'store')->name('categoria.store');
            Route::get('/novacategoria', 'criarNovo')->name('categoria.criarNovo');
            Route::delete('/categorias/{id}', 'deletarCategoria')->name('categoria.deletarCategoria');
        });
    });

    Route::controller(PerfilsController::class)->group(function () {
        Route::get('/perfils', 'index')->name('perfil.index');
        Route::get('/perfils/{id}', 'buscarPerfilPorId')->name('perfil.buscarPerfilPorId');
        Route::put('/perfils/{id}', 'atualizarPerfils')->name('perfil.atualizarPerfils');
        Route::post('/perfils', 'store')->name('perfil.store');
        Route::get('/novoperfil', 'criarNovo')->name('perfils.criarNovo');
        Route::delete('/perfils/{id}', 'deletarPerfil')->name('perfil.deletarPerfil');
    });

    Route::controller(UsuarioController::class)->group(function () {
        Route::get('/usuario', 'index')->name('usuario.index');
        Route::get('/usuario/{id}', 'buscarUsuarioPorId')->name('usuario.buscarUsuarioPorId');
        Route::put('/usuario/{id}', 'atualizarUsuario')->name('usuario.atualizarUsuario');
        Route::post('/usuario', 'store')->name('usuario.store');
        Route::get('/novousuario', 'criarNovo')->name('usuario.criarNovo');
        Route::delete('/usuario/{id}', 'deletarUsuario')->name('usuario.deletarUsuario');
    });

    Route::controller(CargosController::class)->group(function () {
        Route::get('/cargos', 'index')->name('cargos.index');
        Route::get('/cargos/{id}', 'buscarCargoPorId')->name('cargos.buscarCargoPorId');
        Route::put('/cargos/{id}', 'atualizarCargo')->name('cargos.atualizarCargo');
        Route::post('/cargos', 'store')->name('cargos.store');
        Route::get('/novocargo', 'criarNovo')->name('cargos.criarNovo');
        Route::delete('/cargos/{id}', 'deletarCargo')->name('cargos.deletarCargo');
    });

    Route::controller(PlanosController::class)->group(function () {
        Route::get('/planos', 'index')->name('planos.index');
        Route::get('/planos/{id}', 'buscarPlanoPorId')->name('planos.buscarPlanoPorId');
        Route::put('/perfils/{id}', 'atualizarCargo')->name('planos.atualizarCargo');
        Route::post('/planos', 'store')->name('planos.store');
        Route::get('/novoplano', 'criarNovo')->name('planos.criarNovo');
        Route::delete('/planos/{id}', 'deletarPlanos')->name('planos.deletarPlanos');
    });

    Route::controller(ProdutosController::class)->group(function () {
        Route::any('/produtos', 'index')->name('produtos.index');
        Route::get('/produtos/{id}', 'buscarProdutoPorId')->name('produtos.buscarProdutoPorId');
        Route::put('/produtos/{id}', 'atualizarProduto')->name('produtos.atualizarProduto');
        Route::post('/produtos', 'store')->name('produtos.store');
        Route::get('/novoproduto', 'criarNovo')->name('produtos.criarNovo');
        Route::delete('/produtos/{cod}', 'deletarProduto')->name('produtos.deletarProduto');
    });

    Route::controller(PedidoVendaController::class)->group(function () {
        Route::any('/pedidovenda', 'index')->name('pedidovenda.index');
        Route::get('/novavenda', 'criarNovo')->name('pedidovenda.criarNovo');
        Route::post('/novavenda', 'store')->name('pedidovenda.store');
        Route::get('/buscaprod', 'buscaprod')->name('pedidovenda.buscaprod');
        Route::get('/verificaEstoque', 'verificaEstoque')->name('pedidovenda.verificaEstoque');
    });

    Route::controller(DashboardController::class)->group(function () {
        Route::any('/dashboard', 'index')->name('dashboard.index');
    });

    Route::controller(AuthController::class)->group(function () {
        Route::get('/logout', 'logout')->name('auth.logout');
    });
});
//Route::get('/clientes', function () {
//
//
//
//})->middleware(['auth', 'verified'])->name('dashboard');

require __DIR__ . '/auth.php';


//Route::controller(PermissoesController::class)->group(function () {
//    Route::get('/permissoes', 'index');
//    Route::get('/permissoes/{id}', 'buscarPermissaoPorId');
//    Route::post('/permissoes', 'store');
//    Route::delete('/permissoes/{id}', 'deletarPermissoes');
//});
//
//Route::controller(PermissaoPerfilController::class)->group(function () {
//    Route::get('perfil/{id}/permissao', 'permissao');
//    Route::post('perfil/{id}/permissao/store', 'attachPermissionProfile');
//    Route::get('perfil/{id}/permissao/{idPermission}/detach', 'detachPermissionProfile');
//});
//

//Route::controller(PlanoPerfilController::class)->group(function () {
//    Route::get('perfil/{id}/plano', 'plano');
//    Route::post('perfil/{id}/plano/store', 'attachPlanoProfile');
//    Route::get('perfil/{id}/plano/{idPlano}/detach', 'detachPlanoProfile');
//});


//});
